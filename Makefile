RTBENCH_PATH=..
include ${RTBENCH_PATH}/generator/Makefile

PGMS=bench/kernel/minver/minver bench/kernel/binarysearch/binarysearch bench/kernel/bitcount/bitcount bench/kernel/bitonic/bitonic bench/kernel/bsort/bsort bench/kernel/complex_updates/complex_updates bench/kernel/cosf/cosf bench/kernel/countnegative/countnegative bench/kernel/cubic/cubic bench/kernel/deg2rad/deg2rad bench/kernel/fac/fac bench/kernel/fft/fft bench/kernel/filterbank/filterbank bench/kernel/fir2dim/fir2dim bench/kernel/iir/iir bench/kernel/insertsort/insertsort bench/kernel/isqrt/isqrt bench/kernel/jfdctint/jfdctint bench/kernel/lms/lms bench/kernel/ludcmp/ludcmp bench/kernel/matrix1/matrix1 bench/kernel/md5/md5 bench/kernel/pm/pm bench/kernel/prime/prime bench/kernel/quicksort/quicksort bench/kernel/rad2deg/rad2deg bench/kernel/recursion/recursion bench/kernel/sha/sha bench/kernel/st/st bench/sequential/adpcm_dec/adpcm_dec bench/sequential/adpcm_enc/adpcm_enc bench/sequential/ammunition/ammunition bench/sequential/anagram/anagram bench/sequential/audiobeam/audiobeam bench/sequential/cjpeg_transupp/cjpeg_transupp bench/sequential/cjpeg_wrbmp/cjpeg_wrbmp bench/sequential/dijkstra/dijkstra bench/sequential/epic/epic bench/sequential/fmref/fmref bench/sequential/g723_enc/g723_enc bench/sequential/gsm_dec/gsm_dec bench/sequential/gsm_enc/gsm_enc bench/sequential/h264_dec/h264_dec bench/sequential/huff_dec/huff_dec bench/sequential/huff_enc/huff_enc bench/sequential/mpeg2/mpeg2 bench/sequential/ndes/ndes bench/sequential/petrinet/petrinet bench/sequential/rijndael_dec/rijndael_dec bench/sequential/rijndael_enc/rijndael_enc bench/sequential/statemate/statemate bench/sequential/susan/susan bench/app/lift/lift bench/app/powerwindow/powerwindow bench/test/cover/cover bench/test/duff/duff bench/test/test3/test3
BASE_SRC=$(wildcard $(RTBENCH_PATH)/generator/*.c)
BASE_O=$(patsubst %.c,%.o,$(BASE_SRC))
override CFLAGS+=-O2 -Wall -g -I$(RTBENCH_PATH)/generator -DGCC
CXXFLAGS=$(CFLAGS)
override LDFLAGS+=-Wl,--no-as-needed


all: $(PGMS)

bench/kernel/minver/minver: bench/kernel/minver/minver.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/binarysearch/binarysearch: bench/kernel/binarysearch/binarysearch.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/bitcount/bitcount: bench/kernel/bitcount/bitcount.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/bitcount/bitcnt_*.c -o $@ $(LDFLAGS)

bench/kernel/bitonic/bitonic: bench/kernel/bitonic/bitonic.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/bsort/bsort: bench/kernel/bsort/bsort.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/complex_updates/complex_updates: bench/kernel/complex_updates/complex_updates.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/cosf/cosf: bench/kernel/cosf/cosf.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/cosf/wcclibm.c -o $@ $(LDFLAGS)

bench/kernel/countnegative/countnegative: bench/kernel/countnegative/countnegative.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/cubic/cubic: bench/kernel/cubic/cubic.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/cubic/wcclibm.c -o $@ $(LDFLAGS)

bench/kernel/deg2rad/deg2rad: bench/kernel/deg2rad/deg2rad.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/fac/fac: bench/kernel/fac/fac.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/fft/fft: bench/kernel/fft/fft.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/fft/fft_input.c -o $@ $(LDFLAGS)

bench/kernel/filterbank/filterbank: bench/kernel/filterbank/filterbank.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/fir2dim/fir2dim: bench/kernel/fir2dim/fir2dim.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/iir/iir: bench/kernel/iir/iir.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/insertsort/insertsort: bench/kernel/insertsort/insertsort.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/isqrt/isqrt: bench/kernel/isqrt/isqrt.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/isqrt/basicmath_libc.c -o $@ $(LDFLAGS)

bench/kernel/jfdctint/jfdctint: bench/kernel/jfdctint/jfdctint.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/lms/lms: bench/kernel/lms/lms.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/ludcmp/ludcmp: bench/kernel/ludcmp/ludcmp.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/matrix1/matrix1: bench/kernel/matrix1/matrix1.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/md5/md5: bench/kernel/md5/md5.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/pm/pm: bench/kernel/pm/pm.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/kernel/pm/pm_input.c bench/kernel/pm/pm_libm.c bench/kernel/pm/pm_stdlib.c -o $@ $(LDFLAGS)

bench/kernel/prime/prime: bench/kernel/prime/prime.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/quicksort/quicksort: bench/kernel/quicksort/quicksort.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) bench/kernel/quicksort/quicksortlibm.c bench/kernel/quicksort/quicksortstdlib.c bench/kernel/quicksort/input.c -o $@ $(LDFLAGS)

bench/kernel/rad2deg/rad2deg: bench/kernel/rad2deg/rad2deg.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/recursion/recursion: bench/kernel/recursion/recursion.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/kernel/sha/sha: bench/kernel/sha/sha.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/kernel/sha/input_small.c bench/kernel/sha/memhelper.c bench/kernel/sha/memcpy.c bench/kernel/sha/memset.c -o $@ $(LDFLAGS)

bench/kernel/st/st: bench/kernel/st/st.o $(BASE_O)
	$(CC) $(CFLAGS) -DEXTENDED_REPORT $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/adpcm_dec/adpcm_dec: bench/sequential/adpcm_dec/adpcm_dec.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/adpcm_enc/adpcm_enc: bench/sequential/adpcm_enc/adpcm_enc.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/ammunition/ammunition: bench/sequential/ammunition/ammunition.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/ammunition/arithm.c bench/sequential/ammunition/ammunition_libc.c bench/sequential/ammunition/bits.c -o $@ $(LDFLAGS)

bench/sequential/anagram/anagram: bench/sequential/anagram/anagram.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/anagram/anagram_input.c bench/sequential/anagram/anagram_stdlib.c -o $@ $(LDFLAGS)

bench/sequential/audiobeam/audiobeam: bench/sequential/audiobeam/audiobeam.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/audiobeam/audiobeamlibm.c bench/sequential/audiobeam/audiobeamlibmalloc.c bench/sequential/audiobeam/audiobeaminput.c -o $@ $(LDFLAGS)

bench/sequential/cjpeg_transupp/cjpeg_transupp: bench/sequential/cjpeg_transupp/cjpeg_transupp.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/cjpeg_wrbmp/cjpeg_wrbmp: bench/sequential/cjpeg_wrbmp/cjpeg_wrbmp.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/cjpeg_wrbmp/input.c -o $@ $(LDFLAGS)

bench/sequential/dijkstra/dijkstra: bench/sequential/dijkstra/dijkstra.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/dijkstra/input.c -o $@ $(LDFLAGS)

bench/sequential/epic/epic: bench/sequential/epic/epic.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/fmref/fmref: bench/sequential/fmref/fmref.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/fmref/wcclibm.c -o $@ $(LDFLAGS)

bench/sequential/g723_enc/g723_enc: bench/sequential/g723_enc/g723_enc.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/gsm_dec/gsm_dec: bench/sequential/gsm_dec/gsm_dec.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/gsm_enc/gsm_enc: bench/sequential/gsm_enc/gsm_enc.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/h264_dec/h264_dec: bench/sequential/h264_dec/h264_dec.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/h264_dec/h264_decinput.c -o $@ $(LDFLAGS)

bench/sequential/huff_dec/huff_dec: bench/sequential/huff_dec/huff_dec.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/huff_enc/huff_enc: bench/sequential/huff_enc/huff_enc.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/mpeg2/mpeg2: bench/sequential/mpeg2/mpeg2.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/ndes/ndes: bench/sequential/ndes/ndes.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/petrinet/petrinet: bench/sequential/petrinet/petrinet.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/rijndael_dec/rijndael_dec: bench/sequential/rijndael_dec/rijndael_dec.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/rijndael_dec/rijndael_dec_libc.c bench/sequential/rijndael_dec/aes.c bench/sequential/rijndael_dec/input_small_enc.c -o $@ $(LDFLAGS)

bench/sequential/rijndael_enc/rijndael_enc: bench/sequential/rijndael_enc/rijndael_enc.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/rijndael_enc/rijndael_enc_libc.c bench/sequential/rijndael_enc/aes.c bench/sequential/rijndael_enc/input_small.c -o $@ $(LDFLAGS)

bench/sequential/statemate/statemate: bench/sequential/statemate/statemate.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/sequential/susan/susan: bench/sequential/susan/susan.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/sequential/susan/input.c bench/sequential/susan/wccfile.c bench/sequential/susan/wcclibm.c bench/sequential/susan/wccmalloc.c -o $@ $(LDFLAGS)

bench/app/lift/lift: bench/app/lift/lift.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/app/lift/liftlibcontrol.c bench/app/lift/liftlibio.c -o $@ $(LDFLAGS)

bench/app/powerwindow/powerwindow: bench/app/powerwindow/powerwindow.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) bench/app/powerwindow/powerwindow_const_params.c bench/app/powerwindow/powerwindow_controlexclusion.c bench/app/powerwindow/powerwindow_debounce.c bench/app/powerwindow/powerwindow_inputs.c bench/app/powerwindow/powerwindow_powerwindow_control.c bench/app/powerwindow/powerwindow_PW_Control_DRV.c bench/app/powerwindow/powerwindow_PW_Control_PSG_BackL.c bench/app/powerwindow/powerwindow_PW_Control_PSG_BackR.c bench/app/powerwindow/powerwindow_PW_Control_PSG_Front.c bench/app/powerwindow/wcclib.c -o $@ $(LDFLAGS)

bench/test/cover/cover: bench/test/cover/cover.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/test/duff/duff: bench/test/duff/duff.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

bench/test/test3/test3: bench/test/test3/test3.o $(BASE_O)
	$(CC) $(CFLAGS) $(LDFLAGS) $< $(BASE_SRC) -o $@ $(LDFLAGS)

clean:
	rm -f $(PGMS) $(BASE_O)
	rm -f bench/*/*/*.o
